

<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <br> <br> <br>
            <div class="alert alert-danger" role="alert"><b>Kompilacja nie powiodła się!</b> <br>
                ${errorCode}</div>
        </div>
    </div>
</div>