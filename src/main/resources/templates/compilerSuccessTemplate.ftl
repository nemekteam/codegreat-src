

<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <br> <br> <br>
            <div class="alert alert-success" role="alert"><b>Kompilacja powiodła się!</b> <br>
                ${output}</div>
        </div>
    </div>
</div>