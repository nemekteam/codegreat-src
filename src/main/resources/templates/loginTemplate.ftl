<link href="css/signin.css" rel="stylesheet">

<div class="container">
        <form class="form-signin">
            <h2 class="form-signin-heading">Zaloguj się</h2>
            <label for="inputLogin" class="sr-only">Nazwa użytkownika</label>
            <input type="email" id="inputLogin" class="form-control" placeholder="Nazwa użytkownika" required autofocus>
            <label for="inputPassword" class="sr-only">Hasło</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Hasło" required>
            <label class = "register-link">
                Nie masz jeszcze konta?

                <button class="btn btn-default btn-xs" onclick="window.location.href='/register'">Zarejestruj się</button>

            </label>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj się</button>
        </form>
    </div>
