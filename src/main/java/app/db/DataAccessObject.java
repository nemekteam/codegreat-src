package app.db;

import java.sql.*;

public class DataAccessObject {

    private static final String host = "localhost";
    private static final String username = "root";
    private static final String password = "";
    private static final String driverName = "com.mysql.jdbc.Driver";

    protected Connection conn;
    protected Statement stmt;
    protected PreparedStatement pStmt;

    public DataAccessObject()
    {
        registerDriver();
    }

    private void registerDriver()
    {
        try
        {
            Class.forName(driverName).newInstance();
        }
        catch(ClassNotFoundException ex)
        {
            System.out.println("Error: unable to load driver class!");
            ex.printStackTrace();
            System.exit(1);
        }
        catch (InstantiationException e)
        {
            System.out.println("Error: unable to instantiate driver!");
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            System.out.println("Error: access problem while loading!");
            e.printStackTrace();
        }
    }

    private void connectToDatabase()
    {
        try
        {
            conn = DriverManager.getConnection("jdbc:mysql://"+host+"?user="+username+"&password="+password);
            System.out.println("Done!");
        }
        catch (SQLException e)
        {
            System.out.println("Error: unable to connect with SQL database!");
            e.printStackTrace();
        }
    }

    private void disconnectFromDatabase()
    {
        try
        {
            conn.close();
        }
        catch (SQLException e)
        {
            System.out.println("Error: unable to close connection");
            e.printStackTrace();
        }
    }

    private void createPreparedStatement(String sql)
    {
        try
        {
            pStmt = conn.prepareStatement(sql);
        }
        catch (SQLException e)
        {
            System.out.println("Error: unable to create prepared statement");
            e.printStackTrace();
        }
    }
    private void closePreparedStatement()
    {
        try
        {
            if(!pStmt.isClosed())
            {
                pStmt.close();
            }
        }
        catch (SQLException e)
        {
            System.out.println("Error: unable to close prepared statement");
            e.printStackTrace();
        }
    }

    private void createStatement()
    {
        try
        {
            stmt = conn.createStatement();
        }
        catch (SQLException e)
        {
            System.out.println("Error: unable to create statement");
            e.printStackTrace();
        }
    }
    private void closeStatement()
    {
        try
        {
            if(!stmt.isClosed())
            {
                stmt.close();
            }
        }
        catch (SQLException e)
        {
            System.out.println("Error: unable to close statement");
            e.printStackTrace();
        }
    }

    protected ResultSet executeQuery(String query)
    {
        connectToDatabase();
        createPreparedStatement(query);
        ResultSet rs = null;
        try
        {
            rs = pStmt.executeQuery();
        }
        catch(SQLException e)
        {
            System.out.println("Error: unable to get ResultSet");
            e.printStackTrace();
        }
        closePreparedStatement();
        disconnectFromDatabase();
        return rs;
    }

}
