package app.controller;

import app.util.Path;
import spark.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserController
{
    public ModelAndView serveLoginPage(Request request, Response response)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put(Path.Template.VAR_NAME, Path.Template.LOGIN_TEMPLATE);

        return new ModelAndView(model, Path.Template.INDEX_LAYOUT);
    }

    public ModelAndView serveRegisterPage(Request request, Response response)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put(Path.Template.VAR_NAME, Path.Template.REGISTER_TEMPLATE);

        return new ModelAndView(model, Path.Template.INDEX_LAYOUT);
    }

    public ModelAndView postRegisterPage(Request request, Response response)
    {
        Map<String, Object> model = new HashMap<String, Object>();




        model.put(Path.Template.VAR_NAME, Path.Template.REGISTER_TEMPLATE);

        return new ModelAndView(model, Path.Template.INDEX_LAYOUT);
    }

    private ArrayList<String> register(String login, String name, String surname, String email, String password)
    {
        return null;
    }
}