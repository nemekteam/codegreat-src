package app.controller;

import app.util.Path;
import spark.*;

import java.util.HashMap;
import java.util.Map;

public class HomeController
{
    public ModelAndView serveIndexPage(Request request, Response response)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put(Path.Template.VAR_NAME, Path.Template.INDEX_TEMPLATE);

        return new ModelAndView(model, Path.Template.INDEX_LAYOUT);
    }

}
