package app.controller;

import app.util.Path;
import spark.ModelAndView;
import spark.Request;
import spark.Response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class CompilerController
{
    public ModelAndView serveCompilerPage(Request request, Response response)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put(Path.Template.VAR_NAME, Path.Template.COMPILER_TEMPLATE);

        return new ModelAndView(model, Path.Template.INDEX_LAYOUT);
    }

    public ModelAndView postCompiler(Request request, Response response)
    {
        Map<String, Object> model = new HashMap<String, Object>();
        String code = request.queryParams("compilerText");
        try
        {
            PrintWriter writer = new PrintWriter("code.cpp", "UTF-8");
            writer.print(code);
            writer.close();
            ProcessBuilder builder = new ProcessBuilder(
                    "cmd.exe", "/c", "cd \"C:\\MinGW\\bin\" && gcc C:\\Users\\Łukasz\\Desktop\\codegreat-src\\code.cpp -lstdc++ -o program");
            builder.redirectErrorStream(true);
            Process p = builder.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            String errorCode = "";
            while (true)
            {
                line = r.readLine();
                if (line == null)
                {
                    break;
                }
                errorCode += line + "<br>";
            }

            if (errorCode == "")
            {
                model.put(Path.Template.VAR_NAME, Path.Template.COMPILER_SUCCESS_TEMPLATE);
                System.out.println("SUKCES KOMPILACJI");
                ProcessBuilder builder2 = new ProcessBuilder(
                        "cmd.exe", "/c", "cd \"C:\\MinGW\\bin\" && program.exe");
                builder2.redirectErrorStream(true);
                Process p2 = builder2.start();
                BufferedReader r2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
                String output = "";
                while (true)
                {
                    line = r2.readLine();
                    if (line == null)
                    {
                        break;
                    }
                    output += line + "<br>";
                }
                model.put("output", output);

            }
            else
            {
                errorCode = errorCode.replaceAll("�ukasz", " ");
                System.out.println("ERROR");
                System.out.println(errorCode);
                model.put(Path.Template.VAR_NAME, Path.Template.COMPILER_FAILED_TEMPLATE);
                model.put("errorCode", errorCode);
            }

        }
        catch (IOException e)
        {
            // do something
        }



        return new ModelAndView(model, Path.Template.INDEX_LAYOUT);
    }

}
