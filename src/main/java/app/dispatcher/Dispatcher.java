package app.dispatcher;

import app.controller.CompilerController;
import app.controller.HomeController;
import app.controller.UserController;
import app.util.Path;
import sun.util.resources.zh.CalendarData_zh;
import template.FreeMarkerEngine;
import static spark.Spark.*;

public class Dispatcher
{
    HomeController homeController = new HomeController();
    UserController userController = new UserController();
    CompilerController compilerController = new CompilerController();

    public void dispatch()
    {
        staticFileLocation("/templates");
        get(Path.Web.INDEX, homeController::serveIndexPage ,  new FreeMarkerEngine());
        get(Path.Web.LOGIN, userController::serveLoginPage, new FreeMarkerEngine());
        get(Path.Web.REGISTER, userController::serveRegisterPage, new FreeMarkerEngine());
        post(Path.Web.REGISTER, userController::postRegisterPage, new FreeMarkerEngine());
        get(Path.Web.COMPILER, compilerController::serveCompilerPage ,  new FreeMarkerEngine());
        post(Path.Web.COMPILER, compilerController::postCompiler ,  new FreeMarkerEngine());



    }
}
