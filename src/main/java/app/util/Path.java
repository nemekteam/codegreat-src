package app.util;

public class Path
{

    public static class Web
    {
        public static final String INDEX = "/";
        public static final String REGISTER = "/register";
        public static final String LOGIN = "/login";
        public static final String LOGOUT = "/logout";
        public static final String FORGOTPASSWORD = "/forgotPassword";
        public static final String RESETPASSWORD = "/resetPassword";
        public static final String ACCOUNTACTIVATION= "/accountActivation";
        public static final String HOME = "/home" ;
        public static final String COMPILER = "/compiler";


    }

    public static class Template
    {
        public static final String VAR_NAME = "template";
        public static final String INDEX_LAYOUT = "indexLayout.ftl";
        public static final String HOME_LAYOUT = "homeLayout.ftl";

        public static final String INDEX_TEMPLATE = "indexTemplate.ftl";
        public static final String HOME_TEMPLATE = "homeTemplate.ftl";
        public static final String LOGIN_TEMPLATE = "loginTemplate.ftl";
        public static final String REGISTER_TEMPLATE = "registerTemplate.ftl";
        public static final String FORGOT_PASSWORD_TEMPLATE= "forgotPasswordTemplate.ftl";
        public static final String REGISTER_SUCCESS_TEMPLATE = "registerSuccessTemplate.ftl";
        public static final String REGISTER_FAILED_TEMPLATE = "registerFailedTemplate.ftl" ;
        public static final String COMPILER_TEMPLATE = "compilerTemplate.ftl";
        public static final String COMPILER_SUCCESS_TEMPLATE = "compilerSuccessTemplate.ftl";
        public static final String COMPILER_FAILED_TEMPLATE = "compilerFailedTemplate.ftl";




    }

}